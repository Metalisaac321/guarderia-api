using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using guarderiaApi.dto;
using guarderiaApi.models;
using MySql.Data.MySqlClient;

namespace guarderiaApi.repositories
{
    public interface IUsuarioRepository
    {
        Task<List<Usuario>> findAll();
        Task<bool> create(CreateUsuarioDto model);
        Task<Usuario> findById(string id);
        Task<bool> update(UpdateUsuarioDto model);
        Task<bool> delete(int id);
    }

    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly IDbConnection conn;

        public UsuarioRepository(string connectionString)
        {
            conn = new MySqlConnection(connectionString);
        }

        public async Task<List<Usuario>> findAll()
        {
            var calis = (await conn.GetAllAsync<Usuario>()).AsList();
            return calis;
        }

        public async Task<bool> create(CreateUsuarioDto model)
        {
            var id = await conn.InsertAsync(model);
            return true;
        }

        public async Task<Usuario> findById(string id)
        {
            var video = await conn.GetAsync<Usuario>(id);
            return video;
        }

        public async Task<bool> update(UpdateUsuarioDto model)
        {
            return await conn.UpdateAsync(model);
        }

        public async Task<bool> delete(int id)
        {
            return await conn.DeleteAsync(new Usuario { Id = id });
        }

        public async Task<List<Usuario>> findByEmail(string email)
        {
            string sql = $"SELECT Email FROM Usuario WHERE Email like '${email}'";
            return (await conn.QueryAsync<Usuario>(sql)).AsList();
        }

    }
}