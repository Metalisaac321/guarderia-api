using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace guarderiaApi.dto
{
    [Table("Usuario")]
    public class UpdateUsuarioDto
    {   
        [Required]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string TipoUsuario { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

    }
}