﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using guarderiaApi.dto;
using guarderiaApi.models;
using guarderiaApi.repositories;
using Microsoft.AspNetCore.Mvc;

namespace guarderiaApi.controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private UsuarioRepository usuarioRepository;

        public UsuarioController(UsuarioRepository usuarioRepository)
        {
            this.usuarioRepository = usuarioRepository;
        }

        // GET api/usuario
        [HttpGet]
        public async Task<ActionResult<List<Usuario>>> Get()
        {
            return await usuarioRepository.findAll();
        }

        // GET api/usuario/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Usuario>> Get(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return BadRequest();
            }
            var usuario = await usuarioRepository.findById(id);
            return usuario;
        }

        // Post api/usuario
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateUsuarioDto usuario)
        {
            var buscarUsuario = await usuarioRepository.findByEmail(usuario.Email);
            if(buscarUsuario.Count > 0)
            {
                return Conflict("Ya existe un usuario registrado con ese usuario");
            }
            var creado = await usuarioRepository.create(usuario);
            if (creado)
            {
                return Ok();
            }
            else
            {
                return Conflict();
            }
        }

        // PUT api/usuario/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UpdateUsuarioDto value)
        {
            var modificado = await usuarioRepository.update(value);
            if (modificado)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        // DELETE api/usuario/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var borrado = await usuarioRepository.delete(id);
            if (borrado)
            {
                return Ok();
            }
            else
            {
                return Conflict();
            }
        }

        // GET api/usuario/enviarNotificacion
        [HttpGet("enviarNotificacion")]
        public async Task<ActionResult> enviarNotificacion()
        {
            var registrationTokens = new List<string>()
            {
                "8ZJrlyp5Vk:APA91bH9dZ6IOxXFx3Ronlf-nwFjeIwGAdi7u-o32yOGfu43DM2l--UHq8FndKMZb2_qdFAw2eCZBJhzfQ2o0QboCjR2-UbdvfjHnlkMLifZfHqnMsaTVUrTH8G2PyNFcf04DzVyWQiz",
                "cXNk1XFuLKs:APA91bFPPklXY_8ekW3XfIcDPYfMAm8BvHLvYnVXulFRIxOX2bePr-oMrzrRJ4gTk5pf5-Z4VCbqu6TaR7CmjVm06g-95m2T8H66wuFw4dyu72nREsql4yrt2vl7NUEeDtfcBicKCtuA",
            };

            // See documentation on defining a message payload.
            var message = new MulticastMessage()
            {
                Tokens = registrationTokens,
                Data = new Dictionary<string, string>()
                {
                    { "mensajito", "hola amigos estan bien?" },
                },
            };
            try
            {
                var response = await FirebaseMessaging.DefaultInstance.SendMulticastAsync(message);
                if (response.FailureCount > 0)
                {
                    var failedTokens = new List<string>();
                    for (var i = 0; i < response.Responses.Count; i++)
                    {
                        if (!response.Responses[i].IsSuccess)
                        {
                            failedTokens.Add(registrationTokens[i]);
                        }
                    }
                    return BadRequest($"List of tokens that caused failures: {failedTokens}");
                }
                else
                {
                    return Ok("Se enviaron las notificaciones");
                }
            }
            catch (FirebaseException error)
            {
                Console.WriteLine("Error: " + error);
                return NotFound("Error en firebase");
            }

        }
    }
}
