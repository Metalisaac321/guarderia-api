using System;
using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace guarderiaApi.models
{
    [Table("Usuario")]
    public class Usuario
    {   
        [Key]
        public int Id { get; set; }
        public string TipoUsuario { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
    }
}