﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using guarderiaApi.repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using Google.Apis.Auth.OAuth2;

namespace guarderiaApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var conexionMySql = Configuration.GetConnectionString("ConnectionString");
            services.AddTransient<UsuarioRepository>(f =>
                new UsuarioRepository(conexionMySql)
            );
            services.AddMvc(o =>{
            o.OutputFormatters.RemoveType(typeof(HttpNoContentOutputFormatter));
            o.Filters.Add(typeof(ValidateModelStateAttribute));
            o.ModelMetadataDetailsProviders.Add(new RequiredBindingMetadataProvider());
            o.OutputFormatters.Insert(0, new HttpNoContentOutputFormatter
            {
            TreatNullValueAsNoContent = false
        });
        }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

        // Inicializar el sdk de firebase
        FirebaseApp.Create(new AppOptions()
        {
            Credential = GoogleCredential.FromFile("firebase/serviceAccountKey.json"),
        });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseMvc();
    }
}
}
