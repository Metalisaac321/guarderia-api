-- auto-generated definition
create table if not exists Usuario
(
    Id          int auto_increment
        primary key,
    TipoUsuario varchar(50)  default ''                null,
    Nombre      varchar(100) default ''                null,
    Email       varchar(100) default ''                null,
    Password    varchar(100) default ''                null,
    UpdatedAt   timestamp    default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
    CreatedAt   timestamp    default CURRENT_TIMESTAMP null
);

